O grupo, após conversar entre se e revisar os requisitos (visto que os testes tem que ser fortemente baseados neles), resolveu como iria abordar os testes em cada uma de suas dimensões.

- Nível do teste

Para essa dimensão, o grupo concordou que o essencial seria o **teste unitário**, pois, assim, o desenvolvedor mais familiarizado com o seu código pode testar suas partes mais simples sem muitos problemas. Ainda que mais pra frente seja possível à realização de um teste de integração, devido ao perfil do software desenvolvido, para confirmar que todas as partes estão funcionando.  Outro método discutido no grupo também foi o do teste de aceitação para validar com o usuário final a solução. 

- Tipo de teste

Quanto ao tipo de teste, o grupo julgou que o mais adequado ao software seria o **teste de funcionalidade**, pois, o aplicativo não apresenta nenhuma condição crítica de estresse ou segurança, porém, o software tem uma necessidade muito forte de apresentar uma experiência agradável para usuário, ou seja, ser funcional. Agradável nesse contexto é definido pelo grupo como: fácil navegação e operabilidade, design clean, interface intuitiva (o usuário não deve ter que pensar para achar as funcionalidades principais e no máximo de 5 segundos para achar funcionalidades secundárias; podem ser exemplos de requisitos a serem testados nessa dimensão).


- Técnica de teste

Pelo perfil do software, o grupo resolveu escolher uma técnica da indústria mais alinhada com o tipo e com o budget da nossa equipe (no caso, apenas muitas horas de finais de semana e nenhum tostão). Por esse motivo, priorizamos o **teste funcional**. Ou seja, o aplicativo será testado sem acesso ao código fonte, focando apenas em requisitos de desempenho e usabilidade, ou seja, perpassar todos os requisitos para saber se foram atendidos com a qualidade esperada.