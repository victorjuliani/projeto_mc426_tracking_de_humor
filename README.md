Protótipo do projeto
---
O projeto possui um protótipo de alta fidelidade feito no figma, que pode ser acessado por este [Link](https://www.figma.com/proto/OelIR7j23IbhYpqlsSo32O1g/Humority-MC426?node-id=0%3A1&scaling=scale-down).


Testes com Usuários
---
Foram feitos testes de usabilidade com os usuários reais utilizando o protótipo para propor mudanças para o aplicativo que melhorem a experiência do usuário.
Durante os testes, foi observado o que os usuários faziam ao utilizar o aplicativo. A grande maioria dos usuários clicavam em lugares não clicáveis na esperança de que fossem um botão.
Além disso, também foi verificado que, quando ainda não tinha sido adicionado um humor inicial, os usuários ficavam perdidos em como achar o caminho pra utilizar a funcionalidade.
Olhando pra isso, foram propostas algumas mudanças na página principal, principalmente no que tange os padrões de design de elementos clicáveis e não clicáveis.
A seguir, as fotos de comparação da mudança:

**Versão antiga**

![HomeAntiga.jpg](https://i.ibb.co/k13YcdF/Home-sem-hist-ria.png)

**Versão nova**

![HomeNova.jpg](https://i.ibb.co/x79BC5N/Home-sem-hist-ria-1.png)


Arquitetura
---

Para a arquitetura do nosso software será necessário:
- separar a interação com o usuário da lógica de programação
- fazer com que ele seja testável
- estruturá-lo de forma modular (diferenciar responsabilidades, encapsular classes)


Então, o estilo arquitetural adotado pelo grupo será MVP (Model View Presenter). Abaixo como camada irá trabalhar:

![Arquitetura.jpg](https://i.imgur.com/lC1dFBx.jpg)

- **View**: Não vai lidar com a lógica de negócios, apenas com o que será apresentado ao usuário e se ligará ao Presenter. Ou seja, poderá conter 2 módulos, um de interface e um de comunicação entre camadas. Informações que não tem a necessidade de ser armazenadas em tempo real serão tratadas temporariamente por essa camada, para que depois sejam repassadas ao presenter. Ou seja, essa camada não tem permissão de acesso ao disco. O ideal é deixar essa camada o mais dummy possível, para que o grosso dos testes fique na presenter.
    Por exemplo, enquanto o usuário anota informações em seu diário, o texto será armazenado pelo View até que clique em salvar, a partir daí será salvo definitivamente pelas outras camadas.
    Outro exemplo, é que o view deve mostrar as atividades selecionadas que irão para o diário. Note, que da mesma forma que a anterior, ela mantém os dados em memória, mas não em disco. 

- **Presenter**: fica entre o View e o Model. A partir do que recebe do model, controlará o que está acontecendo no View (sem entrar em detalhes de como isso deve ser feito, já que isso é responsabilidade do View) e reagirá às interações do usuário informando o Model (caso necessário). Em outras palavras, a presenter é a camada que serve como comunicadora entre as outras duas. Por isso, ela não pode ter permissão de acessar nem o view nem o model, mas deve ser capaz de se comunicar com os dois através dos métodos previamente determinados. ALém disso, as funções de lógica são implementadas nessa camada, logo o aplicativo deve ser capaz de funcionar somente com essa camada (ou pelo menos, toda sua lógica); 
    Por exemplo, a camada vai mandar ações para o View como "mostrar registros anteriores" ou "mostrar mensagem de erro" sem especificar se essa mensagem aparecerá no topo ou no fim da página.

- **Model**: é a camada responsável por gerenciar o armazenamento de dados e controles de estado do React Native. Sendo assim, essa camada não se faz presente em todas as funções, mas apenas naquelas que precisarão acessar os dados em disco.
    Um exemplo disso é pegar o número de atividades diárias e salvar no local storage do dispositivo.
    
Para que funcione, iremos unir arquivos M,V ou P com um arquivo index e uma classe ConnectComponnet.